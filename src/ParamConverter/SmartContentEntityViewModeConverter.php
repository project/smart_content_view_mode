<?php
namespace Drupal\smart_content_view_mode\ParamConverter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

class SmartContentEntityViewModeConverter implements ParamConverterInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * Creates a new EntityRevisionParamConverter instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $view_mode_id = $defaults['entity_type_id'] . '.' . $value;

    if(isset($defaults['bundle_entity_type']) && $defaults[$defaults['bundle_entity_type']]->id()) {
      $bundle = $defaults[$defaults['bundle_entity_type']]->id();
    } else {
      $bundle = $defaults['bundle_entity_type'];
    }
    $id = 'smart_content_view_mode.' . $defaults['entity_type_id']  . '.' . $bundle  . '.' . $value;
    if($view_mode_entity = \Drupal::entityTypeManager()
      ->getStorage('entity_view_mode')->load($view_mode_id)) {
      if (!$entity = \Drupal::entityTypeManager()
        ->getStorage('smart_variation_set')
        ->load($id)) {
        $entity = \Drupal::entityTypeManager()
          ->getStorage('smart_variation_set')
          ->create(['id' => $id]);
      }
      if(!$entity->getVariationSetType()) {
        $configuration = [
          'variation_set_type_settings' => [
            'context' => [
              'entity_type_id' => $defaults['entity_type_id'],
              'bundle_id' => $bundle,
              'view_mode_id' => $value
            ]
          ]
        ];
        $smart_variation_set_type = \Drupal::service('plugin.manager.smart_content.variation_set_type')->createInstance('view_mode', $configuration, $entity);
        $smart_variation_set_type->entity = $entity;
        $entity->setVariationSetType($smart_variation_set_type);
      }
      return $entity;
    }
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return isset($definition['type']) && $definition['type'] = 'smart_content_view_mode';
  }

}