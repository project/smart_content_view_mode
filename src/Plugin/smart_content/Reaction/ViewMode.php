<?php

namespace Drupal\smart_content_view_mode\Plugin\smart_content\Reaction;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\smart_content\Annotation\SmartReaction;
use Drupal\smart_content\Form\SmartVariationSetForm;
use Drupal\smart_content\Reaction\ReactionBase;
use Drupal\smart_content\Reaction\ReactionConfigurableBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SmartReaction(
 *   id = "view_mode",
 *   label = @Translation("View Mode"),
 * )
 */
class ViewMode extends ReactionConfigurableBase implements ContainerFactoryPluginInterface {
  protected $configFactory;

  protected $entityManager;
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('config.factory')
    );
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityManager $entity_manager, ConfigFactory $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->configFactory = $config_factory;
  }
  /**
   * @inheritdoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    if($options = $this->getViewModeOptions()) {
      $form['view_mode'] = [
        '#type' => 'select',
        '#options' => $options,
        '#required' => true,
      ];
      if(isset($configuration['view_mode'], $options[$configuration['view_mode']])) {
        $form['view_mode']['#default_value'] = $configuration['view_mode'];
      }
    }
    else {
      //@todo: no view modes found.
    }

    return $form;
  }


  function getResponseContent($context = []) {
    $content = [];
    $entity_type_id = $this->entity->getVariationSetType()->getEntityTypeId();
    $view_mode = $this->getConfiguration()['view_mode'];
    if(!empty($context['entity-id'])) {
      if($entity = $this->entityManager->getStorage($entity_type_id)->load($context['entity-id'])) {
        if($entity->access('view')) {
          $view_builder = $this->entityManager->getViewBuilder($entity_type_id);
          $entity->ignore_smart_content = true;
          $content = $view_builder->view($entity, $view_mode);
        }
      }
    }
    return $content;
  }

  public function getViewModeOptions() {
    $options = [];
    $view_modes = $this->getViewModes();
    foreach($this->getDisplays() as $display) {
      if($display->getMode() != $this->entity->getVariationSetType()->getViewModeId()) {
        $options[$display->getMode()] = $view_modes[$display->getMode()]['label'];
      }
    }
    return $options;
  }

  public function getDisplays() {
    $display_id = $this->entity->getVariationSetType()->getEntityTypeId() . '.' .
      $this->entity->getVariationSetType()->getBundleId() . '.default';


    $display_entity_type = EntityViewDisplay::load($display_id)->getEntityTypeId();
    $entity_type = $this->entityManager->getDefinition($display_entity_type);
    $config_prefix = $entity_type->getConfigPrefix();
    $ids = $this->configFactory->listAll($config_prefix . '.' . $this->entity->getVariationSetType()->getEntityTypeId() . '.' . $this->entity->getVariationSetType()->getBundleId() . '.');
    foreach ($ids as $id) {
      $config_id = str_replace($config_prefix . '.', '', $id);
      list(,, $display_mode) = explode('.', $config_id);
      if ($display_mode != 'default') {
        $load_ids[] = $config_id;
      }
    }
    return $this->entityManager->getStorage($display_entity_type)->loadMultiple($load_ids);
  }
  public function getViewModes() {
    return $this->entityManager->getViewModes($this->entity->getVariationSetType()->getEntityTypeId());
  }
}
