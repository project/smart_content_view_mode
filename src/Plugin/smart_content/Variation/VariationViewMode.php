<?php
namespace Drupal\smart_content_view_mode\Plugin\smart_content\Variation;
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 2/28/19
 * Time: 6:08 PM
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\smart_content\Form\SmartVariationSetForm;
use Drupal\smart_content\Variation\VariationBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartVariation(
 *   id = "variation_view_mode",
 *   label = @Translation("View Mode Variation"),
 * )
 */
class VariationViewMode extends VariationBase {

  /**
   * Render API callback: builds the formatter settings elements.
   */
  public function buildWidget(array &$element, FormStateInterface $form_state, array &$complete_form) {
    parent::buildWidget($element, $form_state, $complete_form);
    $wrapper_id = $element['#id'] . '-variation-set-wrapper';

    $element['reactions_config'] = [
      '#type' => 'container',
      '#title' => 'Reactions',
      '#prefix' => '<div id="' . $wrapper_id . '-reactions' . '">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ];

    $element['reactions_config']['reaction_items'] = [
      '#type' => 'container',
    ];


    $reactions = $this->getReactions();
    if(empty($reactions)) {
      $reaction = $this->reactionManager->createInstance('view_mode', [], $this->entity);
      $this->addReaction($reaction);
      $reactions[] = $reaction;
    }

    foreach ($reactions as $reaction_id => $reaction) {
      SmartVariationSetForm::pluginForm($reaction, $element, $form_state, [
        'reactions_config',
        'reaction_items',
        $reaction_id,
        'plugin_form',
      ]);
    }

    return $element;
  }

  function getReactionPluginId() {
    return 'view_mode';
  }

}
