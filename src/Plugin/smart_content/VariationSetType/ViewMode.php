<?php

namespace Drupal\smart_content_view_mode\Plugin\smart_content\VariationSetType;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\smart_content\Form\SmartVariationSetForm;
use Drupal\smart_content\VariationSetType\VariationSetTypeBase;
use Drupal\smart_content_view_mode\Scope\ScopeInterface;
use Drupal\smart_content_view_mode\Scope\ScopeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SmartVariationSetType(
 *   id = "view_mode",
 *   label = @Translation("View Mode"),
 * )
 */
class ViewMode extends VariationSetTypeBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * The SmartViewModeScope plugin manager.
   *
   * @var \Drupal\smart_content_view_mode\Scope\ScopeManager
   */
  protected $scopeManager;

  /**
   * A SmartViewModeScope plugin.
   *
   * @var \Drupal\smart_content_view_mode\Scope\ScopeInterface
   */
  protected $scopeInstance;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.smart_content_view_mode.scope')
    );
  }

  /**
   * ViewMode constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\smart_content_view_mode\Scope\ScopeManager $scope_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ScopeManager $scope_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->scopeManager = $scope_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariationPluginId() {
    return 'variation_view_mode';
  }

  /**
   * {@inheritdoc}
   */
  public function buildWidget(array &$element, FormStateInterface $form_state, array &$complete_form) {
    parent::buildWidget($element, $form_state, $complete_form);
    $element['scope_config'] = [
      '#type' => 'fieldset',
      '#title' => 'Scope of rule',
      '#prefix' => '<div id="' . $element['#id']  . '-scope-set-wrapper" class="scope-container">',
      '#suffix' => '</div>',
      '#description' => 'The scope determines when this rule should be applied.',
      '#tree' => TRUE,
    ];
    //$scope_plugin_id = isset($configuration['variation_set_type_settings']['scope_config']['scope']) ? $configuration['variation_set_type_settings']['scope_config']['scope'] : null;
    $scope = $this->getScope();
    $element['scope_config']['scope'] = [
      '#type' => 'select',
      '#title' => 'Select the scope',
      '#options' => $this->scopeManager->getFormOptions(),
      '#required' => true,
      '#default_value' => $scope->getPluginId(),
    ];
    SmartVariationSetForm::pluginForm($scope, $element, $form_state, [
      'scope_config',
      'scope_settings',
    ]);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $configuration['variation_set_type_settings']['scope_config'] = $form_state->getValue('scope_config');
    $this->setConfiguration($configuration);
  }

  public function updateElementScope(array &$form, FormStateInterface $form_state) {}

  public function updateElementScopeAjax(array &$form, FormStateInterface $form_state) {}

  function getEntityTypeId() {
    return $this->getConfiguration()['variation_set_type_settings']['context']['entity_type_id'];
  }

  function getBundleId() {
    return $this->getConfiguration()['variation_set_type_settings']['context']['bundle_id'];
  }

  function getViewModeId() {
    return $this->getConfiguration()['variation_set_type_settings']['context']['view_mode_id'];
  }

  public function validateReactionRequest($variation_id, $context = []) {
    //@todo: check if entity access view
    //@todo: check if inScope
    //@todo: check if entity_type_id, bundle, and view mode align
    return true;
  }

  public function getScope() {
    $configuration = $this->getConfiguration();
    if(!$this->scopeInstance) {
      $scope_id = isset($configuration['variation_set_type_settings']['scope_config']['scope']) ? $configuration['variation_set_type_settings']['scope_config']['scope'] : 'entity_all';
      $scope_configuration = isset($configuration['variation_set_type_settings']['scope_config']['scope_settings']) ? $configuration['variation_set_type_settings']['scope_config']['scope_settings'] : [];
      $this->scopeInstance = $this->scopeManager->createInstance($scope_id, $scope_configuration);
    }

    return $this->scopeInstance;
  }

  public function setScope(ScopeInterface $scope) {
    $this->scopeInstance = $scope;
  }

  /**
   * @inheritdoc
   */
  public function defaultConfiguration() {
    return [
      'plugin_id' => $this->getPluginId(),
      'variation_set_type_settings' => [
        'context' => $this->configuration['variation_set_type_settings']['context']
      ]
    ];
  }
}
