<?php

namespace Drupal\smart_content_view_mode\Scope;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for SmartContentViewModeScope plugins.
 */
interface ScopeInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
