<?php

namespace Drupal\smart_content_view_mode\Scope;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for SmartContentViewModeScope plugins.
 */
abstract class ScopeBase extends PluginBase implements ScopeInterface {


  // Add common methods and abstract methods for your plugin type here.

}
