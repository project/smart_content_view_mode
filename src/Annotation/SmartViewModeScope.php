<?php

namespace Drupal\smart_content_view_mode\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SmartViewModeScope item annotation object.
 *
 * @see \Drupal\smart_content_view_mode\Scope\ScopeManager
 * @see plugin_api
 *
 * @Annotation
 */
class SmartViewModeScope extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
